/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thingworxclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thingworx.communications.client.ClientConfigurator;
import com.thingworx.communications.client.things.VirtualThing;
import com.thingworx.relationships.RelationshipTypes.ThingworxEntityTypes;
import com.thingworx.types.collections.ValueCollection;

import com.thingworx.communications.client.ConnectedThingClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author KrystianG
 */
public class Client extends ConnectedThingClient {

    private static final Logger LOG = LoggerFactory.getLogger(Client.class);
    private static final String URL = "ws://127.0.0.1:8088/Thingworx/WS";
    private static final String appKey = "4ff39dc7-8128-4147-bc75-0e3fa3e030d6";
    static String[] rooms = {"R1", "R2", "R3", "R4", "R5", "R6","OUTSIDE"};

    public Client(ClientConfigurator config) throws Exception {
        super(config);
    }

    public static void startConnection() {
        ClientConfigurator config = new ClientConfigurator();

        LOG.info("START");
        config.setUri(URL);
        config.setAppKey(appKey);
        config.ignoreSSLErrors(true);
        try {
            Client client = new Client(config);

            client.start();

            while (!client.getEndpoint().isConnected()) {
                Thread.sleep(1000);
                LOG.info("WAIT");
            }
            //ValueCollection params = new ValueCollection();
            Random rand = new Random();
            int i = 0;
            for (String r : rooms) {
                ValueCollection params = new ValueCollection();
                params.SetStringValue("RoomName", r);
                //params.SetStringValue("Description", "ID:"+i);
                i++;
                client.invokeService(ThingworxEntityTypes.Things, "SmartOfficeRoomCreator", "CleateRoomService", params, 5000);
            }
            i = 0;
            for (String r : rooms) {
                SmartOffice thing = new SmartOffice(r, client);
                i++;
                client.bindThing(thing);
            }
//			SmartOffice thing1 = new SmartOffice("TestR1", "test conbection r1", client);
//			SmartOffice thing2 = new SmartOffice("TestR2", "test conbection r2", client);
//			SmartOffice thing3 = new SmartOffice("TestR3", "test conbection r3", client);
//			client.bindThing(thing1);
//			client.bindThing(thing2);
//			client.bindThing(thing3);

            while (!client.isShutdown()) {
                // Loop over all the Virtual Things and process them
                if (client.isConnected()) {
                    LOG.info("SEND");
                    for (VirtualThing thing : client.getThings().values()) {
                        try {
                            thing.processScanRequest();
                        } catch (Exception eProcessing) {
                            System.out.println("Error Processing Scan Request for [" + thing.getName() + "] : " + eProcessing.getMessage());
                        }
                    }
                    LOG.info("SLEEP");
                    Thread.sleep(5000);
                }
            }
            LOG.info("END");

        } catch (Exception e) {
            LOG.info("ERROR");
            e.printStackTrace();
        }

    }

    
}
