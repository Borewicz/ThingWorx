/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thingworxclient;

import com.thingworx.communications.client.ConnectedThingClient;
import com.thingworx.communications.client.things.VirtualThing;

import org.slf4j.Logger;

import com.thingworx.metadata.annotations.ThingworxEventDefinition;
import com.thingworx.metadata.annotations.ThingworxEventDefinitions;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinition;
import com.thingworx.metadata.annotations.ThingworxPropertyDefinitions;
import com.thingworx.metadata.annotations.ThingworxServiceDefinition;
import com.thingworx.metadata.annotations.ThingworxServiceParameter;
import com.thingworx.metadata.annotations.ThingworxServiceResult;
import com.thingworx.metadata.collections.FieldDefinitionCollection;
import com.thingworx.relationships.RelationshipTypes.ThingworxEntityTypes;
import com.thingworx.types.BaseTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.constants.CommonPropertyNames;
import com.thingworx.types.primitives.DatetimePrimitive;
import com.thingworx.types.primitives.LocationPrimitive;
import com.thingworx.types.primitives.NumberPrimitive;
import com.thingworx.types.primitives.StringPrimitive;
import com.thingworx.types.primitives.structs.Location;
import com.thingworx.metadata.FieldDefinition;
import com.thingworx.types.primitives.BooleanPrimitive;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

/**
 *
 * @author KrystianG
 */
@ThingworxPropertyDefinitions(properties = {
    @ThingworxPropertyDefinition(name = "temperature", description = "", baseType = "NUMBER",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"})
    ,
                @ThingworxPropertyDefinition(name = "humidity", description = "", baseType = "NUMBER",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"})
    ,
		@ThingworxPropertyDefinition(name = "occupied", description = "", baseType = "BOOLEAN",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"})
    ,
                @ThingworxPropertyDefinition(name = "light", description = "", baseType = "BOOLEAN",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"})
    ,
                @ThingworxPropertyDefinition(name = "climatization", description = "", baseType = "BOOLEAN",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"})
    ,
                    @ThingworxPropertyDefinition(name = "heating", description = "", baseType = "BOOLEAN",
            aspects = {"dataChangeType:VALUE",
                "dataChangeThreshold:0",
                "cacheTime:0",
                "isPersistent:FALSE",
                "isReadOnly:FALSE",
                "pushType:ALWAYS",
                "isFolded:FALSE",
                "defaultValue:0"}),})
public class SmartOffice extends VirtualThing implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(SmartOffice.class);

    private final static String TEST2_FIELD = "test2";
    private final static String TEST1_FIELD = "test1";
    private Double temperature;
    private Double humidity;
    private boolean occupied;
    private boolean light;
    private boolean climatization;
    private boolean heating;

    private String thingName = null;

    public SmartOffice(String name, ConnectedThingClient client) {
        super(name, "", client);
        thingName = name;
        // Populate the thing shape with the properties, services, and events that are annotated in this code
        super.initializeFromAnnotations();
        this.init();
    }

    // From the VirtualThing class
    // This method will get called when a connect or reconnect happens
    // Need to send the values when this happens
    // This is more important for a solution that does not send its properties on a regular basis
    public void synchronizeState() {
        // Be sure to call the base class
        super.synchronizeState();
        // Send the property values to ThingWorx when a synchronization is required
        super.syncProperties();
    }

    public Double getTemperature() {
        return (Double) getProperty("temperature").getValue().getValue();
    }

    public void setTemperature(Double temperature) throws Exception {
        setProperty("temperature", new NumberPrimitive(this.temperature));
    }

    public Double getHumidity() {
        return (Double) getProperty("humidity").getValue().getValue();
    }

    public void setHumidity(Double humidity) throws Exception {
        setProperty("humidity", new NumberPrimitive(this.humidity));
    }

    public boolean isOccupied() {
        return (boolean) getProperty("occupied").getValue().getValue();
    }

    public void setOccupied(boolean occupied) throws Exception {
        setProperty("occupied", new BooleanPrimitive(this.occupied));
    }

    public boolean isLight() {
        return (boolean) getProperty("light").getValue().getValue();
    }

    public void setLight(boolean light) throws Exception {
        setProperty("light", new BooleanPrimitive(this.light));
    }

    public boolean isClimatization() {
        return (boolean) getProperty("climatization").getValue().getValue();
    }

    public void setClimatization(boolean climatization) throws Exception {
        setProperty("climatization", new BooleanPrimitive(this.climatization));
    }

    public boolean isHeating() {
        return (boolean) getProperty("heating").getValue().getValue();
    }

    public void setHeating(boolean heating) throws Exception {
        setProperty("heating", new BooleanPrimitive(this.heating));
    }

    private void init() {

        temperature = 10.0;
        humidity = 60.0;
        occupied = false;
        light = false;
        climatization = false;
        heating = false;
        try {
            this.setPropertyValue("temperature", new NumberPrimitive(temperature));
            this.setPropertyValue("humidity", new NumberPrimitive(humidity));
            this.setPropertyValue("occupied", new BooleanPrimitive(occupied));
            this.setPropertyValue("light", new BooleanPrimitive(light));
            this.setPropertyValue("climatization", new BooleanPrimitive(climatization));
            this.setPropertyValue("heating", new BooleanPrimitive(heating));
        } catch (Exception ex) {
            LOG.error("Could not ser default value for test2", ex);
        }
    }

    @Override
    public void run() {
        try {
            // Delay for a period to verify that the Shutdown service will return
            Thread.sleep(1000);
            // Shutdown the client
            this.getClient().shutdown();
        } catch (Exception ex) {
            LOG.error("Error " + thingName, ex);
        }

    }

    @Override
    public void processScanRequest() throws Exception {
        //get
        Random rand = new Random();
        try {
            double temperature = rand.nextInt()%50;
            double humidity = Math.abs(rand.nextInt()%100);
            boolean occupied = rand.nextBoolean();
            boolean light = rand.nextBoolean();
            boolean clima = rand.nextBoolean();
            boolean heating = rand.nextBoolean();
            if(this.thingName.equals("OUTSIDE")){
                JSONObject json = readJsonFromUrl("https://api.thingspeak.com/channels/285715/feeds.json?results=1&timezone=Europe/");
                JSONObject singleFeed = json.getJSONArray("feeds").getJSONObject(0);
                temperature = singleFeed.getDouble("field1");
                humidity = singleFeed.getDouble("field4");
                occupied=false;
                light=false;
                clima=false;
                heating=false;
            }
            
            
            LOG.debug("Temperatura:" + temperature + " Wilgotnosc:" + humidity);
            this.setPropertyValue("temperature", new NumberPrimitive(temperature));
            this.setPropertyValue("humidity", new NumberPrimitive(humidity));
            this.setPropertyValue("occupied", new BooleanPrimitive(occupied));
            this.setPropertyValue("light", new BooleanPrimitive(light));
            this.setPropertyValue("climatization", new BooleanPrimitive(clima));
            this.setPropertyValue("heating", new BooleanPrimitive(heating));
        } catch (Exception ex) {
            LOG.error("Could not ser default value for test2", ex);
        }

        this.updateSubscribedProperties(5000);
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
